﻿Imports System
Imports System.IO
Imports System.Net.Mail
Imports OpenPop.Common.Logging
Imports OpenPop.Mime
Imports OpenPop.Mime.Decode
Imports OpenPop.Mime.Header
Imports OpenPop.Pop3
Public Class Form1

    Dim start As Integer = My.Settings.Interval
    Public logfile As String = My.Settings.LogFile

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'EmailDataSet.EmailData' table. You can move, or remove it, as needed.
        Me.EmailDataTableAdapter.Fill(Me.EmailDataSet.EmailData)
        Timer1.Start()
        Timer1_Tick(Me, e)
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Dim interval As Integer
        interval = My.Settings.Interval
        Dim logString As String = ""
        Dim ErrString As String = ""
        If start >= interval Then
            'Process Emails
            Using client As Pop3Client = New Pop3Client()
                Try
                    client.Connect(My.Settings.RecvServer, My.Settings.Port, My.Settings.RecvSSL)
                Catch
                    PictureBox3.Image = WindowsApplication1.My.Resources.red_button
                    RichTextBox1.Text = Now & " - Failed To Connect to " & My.Settings.RecvServer & vbNewLine & RichTextBox1.Text
                    logString = logString & Now & " - Failed To Connect to " & My.Settings.RecvServer & vbNewLine
                    GoTo Endit
                End Try
                client.Authenticate(My.Settings.User, My.Settings.Pass)
                'Make sure you are connected to the server
                If client.Connected = True Then
                    RichTextBox1.Text = Now & " - Connected to " & My.Settings.RecvServer & vbNewLine & RichTextBox1.Text
                    logString = logString & Now & " - Connected to " & My.Settings.RecvServer & vbNewLine
                    PictureBox3.Image = WindowsApplication1.My.Resources.green_button
                    Dim messageCount As Integer = client.GetMessageCount()
                    Dim allMessages As List(Of Message) = New List(Of Message)(messageCount)
                    'Only continue if there are emails
                    If messageCount > 0 Then
                        For i As Integer = 1 To messageCount
                            Dim mailitem = client.GetMessage(i)
                            Dim mailpassed As Boolean = False
                            Dim attachfound As Boolean = False
                            RichTextBox1.Text = Now & " - " & "Processing Email From " & mailitem.Headers.From.Address & vbNewLine & RichTextBox1.Text
                            logString = logString & Now & " - " & "Processing Email From " & mailitem.Headers.From.Address & vbNewLine
                            For Each Rule As DataRow In EmailDataSet.EmailData.Rows
                                'Check to see if the address has a rule
                                If mailitem.Headers.From.Address.ToUpper = Rule.Item("Email").ToString.ToUpper Then
                                    mailpassed = True
                                    Dim MessageParts As Integer = mailitem.MessagePart.MessageParts.Count - 1
                                    For a As Integer = 0 To MessageParts
                                        Dim attachment = mailitem.MessagePart.MessageParts(a)
                                        'Make sure the attachment is the correct extention
                                        If attachment.FileName.ToUpper.Contains(Rule.Item("Ext").ToString.ToUpper) Then
                                            attachfound = True
                                            Dim paths = Rule.Item("Dir").ToString.Split(";")
                                            RichTextBox1.Text = Now & " - " & " Attachment " & mailitem.MessagePart.MessageParts(a).FileName & " Found " & vbNewLine & RichTextBox1.Text
                                            logString = logString & Now & " - " & " Attachment " & mailitem.MessagePart.MessageParts(a).FileName & " Found " & vbNewLine
                                            For Each savepath In paths
                                                Dim filePath As String = Path.Combine(savepath, DateTime.Now().ToString("yyyy-MM-dd(hh-mm-ss-FFF)") + "-" + attachment.FileName)
                                                Dim Stream As FileStream = New FileStream(filePath, FileMode.Create)
                                                Dim BinaryStream As BinaryWriter = New BinaryWriter(Stream)
                                                BinaryStream.Write(attachment.Body)
                                                BinaryStream.Close()
                                                RichTextBox1.Text = Now & " - " & "Attachment Saved - " & filePath & vbNewLine & RichTextBox1.Text
                                                logString = logString & Now & " - " & "Attachment Saved - " & filePath & vbNewLine
                                            Next

                                        End If
                                    Next
                                    If attachfound Then
                                        Try
                                            client.DeleteMessage(i)
                                            RichTextBox1.Text = Now & " - " & "Email Deleted" & vbNewLine & RichTextBox1.Text
                                            logString = logString & Now & " - " & "Email Deleted" & vbNewLine

                                        Catch
                                            RichTextBox1.Text = Now & " - " & "Failed to Delete Email" & vbNewLine & RichTextBox1.Text
                                            logString = logString & Now & " - " & "Failed to Delete Email" & vbNewLine
                                            SendEmail(Now & " - " & "Failed to Delete Email from " & mailitem.Headers.From.Address)
                                        End Try
                                    End If
                                Else
                                    start = 1
                                End If

                            Next
                            If attachfound = False Then
                                RichTextBox1.Text = Now & " - Email from " & mailitem.Headers.From.Address & " did not contain a valid attachment" & vbNewLine & RichTextBox1.Text
                                logString = logString & Now & " - Email from " & mailitem.Headers.From.Address & " did not contain a valid attachment" & vbNewLine
                                SendEmail(Now & " - " & "Email from " & mailitem.Headers.From.Address & " with the subject " & mailitem.Headers.Subject.ToString & " did not contain a valid attachment")
                            End If
                            If mailpassed = False Then
                                RichTextBox1.Text = Now & " - Email " & mailitem.Headers.From.Address & " is not in list" & vbNewLine & RichTextBox1.Text
                                logString = logString & Now & " - Email " & mailitem.Headers.From.Address & " is not in list" & vbNewLine
                            End If

                        Next
                        client.Disconnect()
                        client.Dispose()
                        RichTextBox1.Text = Now & " - Disconnected from " & My.Settings.RecvServer & vbNewLine & RichTextBox1.Text
                        logString = logString & Now & " - Disconnected from " & My.Settings.RecvServer & vbNewLine

                        start = 1
                    Else
                        RichTextBox1.Text = Now & " - " & "No Emails to Process" & vbNewLine & RichTextBox1.Text
                        logString = logString & Now & " - " & "No Emails to Process" & vbNewLine
                        client.Disconnect()
                        client.Dispose()
                        RichTextBox1.Text = Now & " - Disconnected from " & My.Settings.RecvServer & vbNewLine & RichTextBox1.Text
                        logString = logString & Now & " - Disconnected from " & My.Settings.RecvServer & vbNewLine
                        start = 1
                    End If
                Else
                    PictureBox3.Image = WindowsApplication1.My.Resources.red_button
                    RichTextBox1.Text = Now & " - Failed To Connect to " & My.Settings.RecvServer & vbNewLine & RichTextBox1.Text
                    logString = logString & Now & " - Failed To Connect to " & My.Settings.RecvServer & vbNewLine
                    SendEmail(Now & " - Failed To Connect to " & My.Settings.RecvServer & " on port " & My.Settings.Port)

                End If
            End Using

        Else
            start = start + 1
        End If
Endit:
        My.Computer.FileSystem.WriteAllText(logfile, logString, True)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Application.Exit()
    End Sub

    Private Sub PictureBox2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox2.Click
        Dim setform As New Settings
        setform.ShowDialog()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        PictureBox3.Image = WindowsApplication1.My.Resources.red_button
        Dim logString As String = ""
        RichTextBox1.Text = Now & " - Process stopped by user" & vbNewLine & RichTextBox1.Text
        logString = logString & Now & " - Process stopped by user" & vbNewLine
        My.Computer.FileSystem.WriteAllText(logfile, logString, True)
        Timer1.Stop()
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        start = My.Settings.Interval
        Dim logString As String = ""
        RichTextBox1.Text = Now & " - Process started by user" & vbNewLine & RichTextBox1.Text
        logString = logString & Now & " - Process started by user" & vbNewLine
        My.Computer.FileSystem.WriteAllText(logfile, logString, True)
        Timer1.Start()
        Timer1_Tick(Me, e)
    End Sub

    Private Sub EmailDataBindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.EmailDataBindingSource.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.EmailDataSet)

    End Sub

    Private Sub PictureBox4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox4.Click
        Me.WindowState = FormWindowState.Minimized
    End Sub
    Private Sub SendEmail(ByVal Err As String)
        Dim logString As String = ""
        Try
            Dim Smtp_Server As New SmtpClient
            Dim e_mail As New MailMessage()
            Smtp_Server.UseDefaultCredentials = False
            Smtp_Server.Credentials = New Net.NetworkCredential(My.Settings.User, My.Settings.Pass)
            Smtp_Server.Port = My.Settings.PortSend
            Smtp_Server.EnableSsl = My.Settings.SendSSL
            Smtp_Server.Host = My.Settings.SendServer

            e_mail = New MailMessage()
            e_mail.From = New MailAddress(My.Settings.SendFrom)
            e_mail.To.Add(My.Settings.ErrorEmail)
            e_mail.Subject = "Email EDI Error"
            e_mail.IsBodyHtml = False
            e_mail.Body = Err
            Smtp_Server.Send(e_mail)
            RichTextBox1.Text = Now & " - Email sent to " & My.Settings.ErrorEmail & vbNewLine & RichTextBox1.Text
            logString = logString & Now & " - Email sent to " & My.Settings.ErrorEmail & vbNewLine
        Catch error_t As Exception
            RichTextBox1.Text = Now & " - Failed sending email to " & My.Settings.ErrorEmail & vbNewLine & RichTextBox1.Text
            logString = logString & Now & " - Failed sending email to " & My.Settings.ErrorEmail & vbNewLine
        End Try
        My.Computer.FileSystem.WriteAllText(logfile, logString, True)
    End Sub
End Class