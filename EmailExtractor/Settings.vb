﻿Public Class Settings
    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        EmailDataTableAdapter.Update(EmailDataSet.EmailData)
        EmailDataSet.AcceptChanges()
        Form1.EmailDataTableAdapter.Fill(Form1.EmailDataSet.EmailData)
        Me.Hide()
    End Sub

    Private Sub Settings_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        My.Settings.Interval = NumericUpDown1.Value
        My.Settings.RecvServer = TextBox1.Text
        My.Settings.Port = NumericUpDown2.Value
        My.Settings.PortSend = NumericUpDown3.Value
        My.Settings.User = TextBox2.Text
        My.Settings.Pass = TextBox3.Text
        My.Settings.LogFile = TextBox4.Text
        Form1.logfile = TextBox4.Text

    End Sub

    Private Sub Settings_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'EmailDataSet.EmailData' table. You can move, or remove it, as needed.
        Me.EmailDataTableAdapter.Fill(Me.EmailDataSet.EmailData)
        NumericUpDown1.Value = My.Settings.Interval
        TextBox1.Text = My.Settings.RecvServer
        NumericUpDown2.Value = My.Settings.Port
        TextBox2.Text = My.Settings.User
        TextBox3.Text = My.Settings.Pass
        TextBox4.Text = My.Settings.LogFile
        TextBox5.Text = My.Settings.ErrorEmail
        TextBox6.Text = My.Settings.SendFrom
        CheckBox1.Checked = My.Settings.SendSSL
        NumericUpDown3.Value = My.Settings.PortSend
    End Sub

    Private Sub EmailDataBindingSource1BindingNavigatorSaveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Validate()
        Me.EmailDataBindingSource1.EndEdit()
        Me.TableAdapterManager.UpdateAll(Me.EmailDataSet)

    End Sub

    Private Sub PictureBox1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles PictureBox1.Click
        If OpenFileDialog1.ShowDialog = DialogResult.OK Then
            TextBox4.Text = System.IO.Path.GetFullPath(OpenFileDialog1.FileName)
        End If
    End Sub
End Class